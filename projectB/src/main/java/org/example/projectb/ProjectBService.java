package org.example.projectb;

import org.example.projecta.ProjectAService;

public class ProjectBService {

    public void doSomethingB() {
        ProjectAService projectAService = new ProjectAService();
        projectAService.doSomethingProjectA();
        System.out.println("this is project B");
    }
}
