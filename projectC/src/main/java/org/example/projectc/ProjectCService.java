package org.example.projectc;

import org.example.projecta.ProjectAService;

public class ProjectCService {

    public void doSomethingC() {
        ProjectAService projectAService = new ProjectAService();
        projectAService.doSomethingProjectA();
        System.out.println("this is project C1");
    }
}
